<div class="layer-one">
	<div class="contact">
		<img src="./dist/images/icon_phone.png" alt="">
		<p>123.123.1234</p>
	</div>
	<div class="quote">
		<a href="javascript:void(0)" class="">
			<img src="./dist/images/icon_quote.png" alt="">
			Request A Quote
		</a>
	</div>
</div>
<div class="layer-two">
	<div class="logo"></div>
	<nav>
		<ul>
			<li><a href="javascript:void(0)">Home</a></li>
			<li><a href="javascript:void(0)">About</a></li>
			<li><a href="javascript:void(0)">Services</a></li>
			<li><a href="javascript:void(0)">Projects</a></li>
			<li><a href="javascript:void(0)">Blog</a></li>
			<li><a href="javascript:void(0)">Contact</a></li>
		</ul>
	</nav>
</div>