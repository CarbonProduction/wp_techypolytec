<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="custom">
					<h1>I AM A POPUP</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>

				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">

			<!-- content -->
			<a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a>
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

